Feature: Filter by different parameters
  As a user
  I should be able to filter items on Home page by Color and Brand

  @p2 @filter
  Scenario: To verify user is able to filter items on the page by Color
    Given user opens Home page
    When user applies Black color filter
    Then black color filter is selected
    And active filter label Black is displayed

  @p2 @filter
  Scenario: To verify user is able to filter items on the page by Brand
    Given user opens Home page
    When user applies Calvin Klein brand filter
    Then Calvin Klein brand filter is selected
    And active filter label Calvin Klein is displayed
    And 20 items are displayed on the page
    And all displayed items belong to brand Calvin Klein



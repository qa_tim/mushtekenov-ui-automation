Feature: Search
  As a user
  I should be able to search items on Home page

  @smoke
  Scenario: To verify search results are displayed
    Given user opens Home page
    When user searches 'jeans'
    Then search results are displayed
    And 20 items are displayed on the page

  @p1
  Scenario Outline: To verify error message is displayed for invalid search keyword
    Given user opens Home page
    When user searches '<keyword>'
    Then error message 'No results found' is displayed

    Examples:
    |keyword   |
    |jeanss    |
    |asdfqwerty|


